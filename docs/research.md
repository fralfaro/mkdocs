# Research & Talks

## **Research**
- Alfaro,F. (2019). Estimation of the reciprocal effects between Happiness and Job Performance in Chile.  
Master thesis. <a href="https://gitlab.com/FAAM/mkdocs/-/blob/master/docs/files/researches/tesis_master.pdf" title="PDF" target="_new"> File: <img src="https://seeklogo.com/images/A/acrobat-file-pdf-logo-37A1BFDE35-seeklogo.com.png"  width="30" height="30"></a>

- Alfaro,F. (2018/2019). University retention about construction engineering students.   
Investigation. <a href="https://gitlab.com/FAAM" title="repository" target="_new"> Repository: <img src="https://cdn.worldvectorlogo.com/logos/gitlab.svg"  width="30" height="30"></a>
  
- Alfaro,F. (2018). Spatial-statistics study for 3D Kriging (laboratory tests for tailings).   
Investigation. <a href="https://gitlab.com/FAAM" title="repository" target="_new"> Repository: <img src="https://cdn.worldvectorlogo.com/logos/gitlab.svg"  width="30" height="30"></a>

- Alfaro,F. (2017). Fraud detection in drinking water consumption.   
Degree thesis. <a href="https://gitlab.com/FAAM/mkdocs/-/blob/master/docs/files/researches/tesis_degree.pdf" title="PDF" target="_new"> File: <img src="https://seeklogo.com/images/A/acrobat-file-pdf-logo-37A1BFDE35-seeklogo.com.png"  width="30" height="30"></a>
  

## **Talks**

- Alfaro,F. (2018). Estimation of the reciprocal effects between Happiness and Job Performance in Chile. **FNE33/CLATSE13 Congress**. 
Guadalajara, MX, October 04, 2018.
<a href="https://gitlab.com/FAAM/mkdocs/-/blob/master/docs/files/talks/FNE33_talk.pdf" title="PDF" target="_new"> Talk:  <img src="https://cdn.shopify.com/s/files/1/1061/1924/products/Thought_Speech_Bubble_Emoji_grande.png?v=1571606035"  width="30" height="30"></a>
<a href="https://gitlab.com/FAAM/mkdocs/-/blob/master/docs/files/talks/FNE33_val.pdf" title="PDF" target="_new"> Validation: <img src="https://pngimage.net/wp-content/uploads/2018/06/paper-emoji-png.png"  width="30" height="30"></a>
