# Teaching

## **Universities Courses**  

### MAT281 - Applications of Mathematics in Engineering

- **University**: Universidad Técnica Federico Santa María (UTFSM)
- **Course**: Applications of Mathematics in Engineering
- **Course abbreviation**: MAT281
- **Prerequisites**: None
- **Academic Unit that teaches it**: Department of Mathematics
- **Training axis**: Applied Engineering and Integration
- **Information**: [file](https://gitlab.com/FAAM/mkdocs/-/blob/master/docs/files/teaching/mat281.pdf)
- **Repository**:<a href="https://github.com/fralfaro/MAT281_2019" title="repository" target="_new"> 2019 <img src="https://github.githubassets.com/images/modules/logos_page/GitHub-Mark.png"  width="30" height="30"></a>,
<a href="https://github.com/fralfaro/MAT281_2020" title="repository" target="_new"> 2020 <img src="https://github.githubassets.com/images/modules/logos_page/GitHub-Mark.png"  width="30" height="30"></a>,
  <a href="https://gitlab.com/FAAM/mat281_2021" title="repository" target="_new"> 2021 <img src="https://cdn.worldvectorlogo.com/logos/gitlab.svg"  width="30" height="30"></a>

  
### Previous Courses

- MAT021 - Calculus and Algebra I (UTFSM)
- MAT022 - Calculus and Algebra II (UTFSM)

## **Personal Courses**  

### Basic Tools

- **Course**: Basic Tools
- **Description**: Basic tools for Data Scientists
- **Repository**: <a href="https://gitlab.com/FAAM/basic_tools" title="repository" target="_new"> Gitlab <img src="https://cdn.worldvectorlogo.com/logos/gitlab.svg"  width="30" height="30"></a>


### Python Introduction

- **Course**: Python Introduction 
- **Description**: Basic concepts about Python
- **Repository**: <a href="https://gitlab.com/FAAM/python_intro" title="repository" target="_new"> Gitlab <img src="https://cdn.worldvectorlogo.com/logos/gitlab.svg"  width="30" height="30"></a>


### Data Structures and Algorithms

- **Course**: Data Structures and Algorithms
- **Description**: Data structures and algorithms with Python
- **Repository**: <a href="https://gitlab.com/FAAM/python_eda" title="repository" target="_new"> Gitlab <img src="https://cdn.worldvectorlogo.com/logos/gitlab.svg"  width="30" height="30"></a>


### Data Manipulation

- **Course**: Data Manipulation
- **Description**: Basic concepts about data manipulation wih Python (Numpy, Panda, Matplotlib)
- **Repository**: <a href="https://gitlab.com/FAAM/python_data_manipulation" title="repository" target="_new"> Gitlab<img src="https://cdn.worldvectorlogo.com/logos/gitlab.svg"  width="30" height="30"></a>

### Machine Learning

- **Course**: Machine Learning 
- **Description**: Basic concepts about Machine Learning (numpy, pandas, scikit-learn)
- **Repository**: <a href="https://gitlab.com/FAAM/python_machine_learning" title="repository" target="_new"> Gitlab<img src="https://cdn.worldvectorlogo.com/logos/gitlab.svg"  width="30" height="30"></a>

### Deep Learning

- **Course**: Deep Learning 
- **Description**: Basic concepts about Deep Learning with Tensorflow and Keras
- **Repository**: <a href="https://gitlab.com/FAAM/python_deep_learning" title="repository" target="_new"> Gitlab <img src="https://cdn.worldvectorlogo.com/logos/gitlab.svg"  width="30" height="30"></a>

### Big Data

- **Course**: Big Data
- **Description**: Basic concepts about Big Data with Pyspark
- **Repository**: <a href="https://gitlab.com/FAAM/python_big_data" title="repository" target="_new"> Gitlab <img src="https://cdn.worldvectorlogo.com/logos/gitlab.svg"  width="30" height="30"></a>

### Software Design I

- **Course**: Software Design I
- **Description**: Basic concepts about software design (version control, testing, and automatic build management)
- **Repository**: <a href="https://gitlab.com/FAAM/python_sdk1" title="repository" target="_new"> Gitlab <img src="https://cdn.worldvectorlogo.com/logos/gitlab.svg"  width="30" height="30"></a>

### Software Design II

- **Course**: Software Design II
- **Description**: Advanced concepts about software design (version control, testing, and automatic build management)
- **Repository**: <a href="https://gitlab.com/FAAM/python_sdk2" title="repository" target="_new"> Gitlab <img src="https://cdn.worldvectorlogo.com/logos/gitlab.svg"  width="30" height="30"></a>
